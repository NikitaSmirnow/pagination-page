const url = "https://jsonplaceholder.typicode.com/comments"; 

let userList = [];

const handleFetchUsers= async ()=> {
  result = await fetch(url).then((response) => response.json()); // получение json

  result.forEach((user)=>{
    userList.push(Object.values(user));
  })
  let itemStart = parseInt(localStorage.getItem("pageNumber") - 1) * quantity;

  userContainer.innerHTML = renderUserList(
    userList.slice(itemStart, itemStart + quantity)
  );

  pageInfo.innerHTML = `Showing ${itemStart + 1} to ${
    itemStart + quantity
  } of ${userList.length} entires`;

  pageNumber.value = localStorage.getItem("pageNumber");


  if (localStorage.getItem('pageNumber') == null) {
    userContainer.innerHTML = renderUserList(userList.slice(0, quantity));
    pageNumber.value = 1;
    pageInfo.innerHTML = `Showing 1 to ${quantity} of ${userList.length} entires`;
    localStorage.setItem("pageNumber", pageNumber.value);
  }
}

handleFetchUsers();

const table = document.getElementById("table"); 


let quantity = 10; // количество элементов на отображение


// test git
const changeQuantity=()=> {
  let select = document.getElementById("quantity"); //get select element

  quantity = +select.value;
  userContainer.innerHTML = renderUserList(userList.slice(0, quantity));

  pageInfo.innerHTML = `Showing ${(pageNumber.value - 1) * quantity + 1} to ${
    (pageNumber.value - 1) * quantity + quantity
  } of ${userList.length} entires`;

  pageNumber.value = 1;

  localStorage.setItem("quantity", quantity);
  return quantity;
} // изменение количества отображаемых элементов на странице


function renderUserList(users) {          //парсит массив в таблицу
  let res = "";

  users.forEach((user, index) => {
    if (user[4].length > 70) {
      user[4] = user[4].slice(0, 70) + "...";
    }
    res += `<div class='id'> ${user[1]}</div>`;
    res += `<div class='name'>${user[2]}</div>`;
    res += `<div class='email'>${user[3]}</div>`;
    res += `<div class='comment'>${user[4]}</div>`;
  });


  return res;
}

 const numberOfPages = (arr, num) => Math.ceil(arr.length / num); //рассчет количества страниц


const userContainer = document.createElement("div"); // div для хранения кжадого элемента
userContainer.setAttribute("class", "elements");

const pageInfo = document.createElement("div"); // div для информации о страницах
pageInfo.setAttribute("class", "page-info");



footer.append(pageInfo);
table.append(userContainer);


const compareById = (a, b) => {
  if (a[1] > b[1]) return 1;
  if (a[1] == b[1]) return 0;
  if (a[1] < b[1]) return -1;
}


const sortById = () => {
  // сортировка по id
  if (userList[0][1] == 1) {
    userList.sort(compareById).reverse();
  } else {
    userList.sort(compareById);
  }


  userContainer.innerHTML = renderUserList(userList.slice(0, quantity));

  idSort = document.getElementById("idSort");
  if (idSort.getAttribute("class") == "sort") {
    // анимация для кнопки сортировки
    idSort.setAttribute("class", "sortDec");
  } else {
    idSort.setAttribute("class", "sort");
  }
}

const searchByName = () => {
  // поиск по имени
  search = document.getElementById("search");
  result = [];

  userList.forEach((user,index) =>{
    if (user[2].includes(search.value)) {
    result.push(user);
  }})
  

  if (result.length == 0) {
    result = [["", "error", "Name not Found", "", ""]];
  }

  userContainer.innerHTML = renderUserList(result.slice(0, quantity));
  pageInfo.innerHTML = `Page ${pageNumber.value} of ${numberOfPages(
    result,
    quantity
  )}`;
}

// кнопки навигации
const pageNumber = document.getElementById("pageNumber");


const toNextPage = () => {
  if (pageNumber.value < numberOfPages(userList, quantity)) {
    userContainer.innerHTML = renderUserList(
      userList.slice(
        pageNumber.value * quantity,
        pageNumber.value * quantity + quantity
      )
    );
    pageInfo.innerHTML = `Showing ${pageNumber.value * quantity + 1} to ${
      pageNumber.value * quantity + quantity
    } of ${userList.length} entires`;
    pageNumber.value++;
    localStorage.setItem("pageNumber", pageNumber.value);
  }
}


const toPreviousPage = () => {
  if (pageNumber.value != 1) {
    pageNumber.value--;
    userContainer.innerHTML = renderUserList(
      userList.slice(
        (pageNumber.value - 1) * quantity,
        (pageNumber.value - 1) * quantity + quantity
      )
    );
    pageInfo.innerHTML = `Showing ${(pageNumber.value - 1) * quantity + 1} to ${
      (pageNumber.value - 1) * quantity + quantity
    } of ${userList.length} entires`;
    localStorage.setItem("pageNumber", pageNumber.value);
  }
}
